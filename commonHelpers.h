// Copyright (c) 2022 - 2023 by Rainer Wahler, RWahler@gmx.net

#ifndef COMMONHELPERS
	#define	COMMONHELPERS

	#include <inttypes.h>
	#include <stdbool.h>

	// CPU Freuquenz und Taktteiler
	#define CPU_FREQ 20000000
	#define CPU_DIV 1

	#define F_CPU (CPU_FREQ / CPU_DIV)

#endif	/* COMMONHELPERS */
