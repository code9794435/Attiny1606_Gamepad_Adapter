// Copyright (c) 2022 - 2023 by Rainer Wahler, RWahler@gmx.net

/* Offene TODOs
 * - Temperaturberechnung zeigt v�llig falsche Werte an
 */

#include "main.h"
#include "commonHelpers.h"
#include "../Library.X/libConvert.h"
#include "../Library.X/libUART.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/cpufunc.h>
#include <avr/wdt.h>
#include <util/delay.h>
#include <string.h>
#include <stdlib.h>

bool b_showDebugOutput = false;
uint8_t u8_GamepadType;
uint8_t u8_SegaMapBtA;
uint8_t u8_SegaMapBtB;
uint8_t u8_SegaMapBtC;
uint8_t u8_SegaMapBtX;
uint8_t u8_SegaMapBtY;
uint8_t u8_SegaMapBtZ;
// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//uint8_t u8_SegaMapBtMode;
uint8_t u8_SegaMapBtStart;
uint8_t u8_SegaButtonUDLR;
uint8_t u8_SegaButtonMZYXSCBA;
volatile uint8_t u8_CD32ButtonUDLR = 0xFF;
volatile uint8_t u8_CD32ButtonSLRGYRB = 0xFF;

void init(void) {
	// CPU Prescaler (/6) deaktivieren, wodurch die CPU direkt mit 20MHz l�uft
	ccp_write_io((void*)&CLKCTRL.MCLKCTRLB, ~CLKCTRL_PEN_bm);

	// LED konfigurieren
	LED_PORT.DIRSET = LED_PIN;
	
	// Sega Port konfigurieren
	SEGA_PORT_SEL.DIRSET = SEGA_OUT_SEL;
	SEGA_PORT1.DIRCLR = SEGA_IN_D0;
	SEGA_PORT1.DIRCLR = SEGA_IN_D1;
	SEGA_PORT1.DIRCLR = SEGA_IN_D2;
	SEGA_PORT1.DIRCLR = SEGA_IN_D3;
	SEGA_PORT2.DIRCLR = SEGA_IN_D4;
	SEGA_PORT2.DIRCLR = SEGA_IN_D5;

	// Select Leitung auf 1 setzen
	SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

	// CD32 Port konfigurieren
	CD32_PORT.DIRSET = CD32_OUT_UP;
	CD32_PORT.DIRSET = CD32_OUT_DOWN;
	CD32_PORT.DIRSET = CD32_OUT_LEFT;
	CD32_PORT.DIRSET = CD32_OUT_RIGHT;
	CD32_PORT.DIRCLR = CD32_IN_SEL;
	CD32_PORT.DIRSET = CD32_INOUT_RED_CLOCK;
	CD32_PORT.DIRSET = CD32_OUT_BLUE_DATA;

	// Leitung auf 1 setzen
	CD32_PORT.OUTSET = CD32_OUT_BLUE_DATA;

	// Interrupt bei fallender Flanke
	CD32_PORT.CD32_IN_SEL_ISR |= PORT_ISC_FALLING_gc;

	// 10Bit Aufl�sung
	ADC0.CTRLA |= ADC_RESSEL_10BIT_gc;

	// 16 Samples akkumulieren
	ADC0.CTRLB |= ADC_SAMPNUM_ACC16_gc;

	// Interne Referenzspannung f�r den AD-Wandler auf 1,1V festlegen
	VREF.CTRLA |= VREF_ADC0REFSEL_1V1_gc;

	// CLK_PER durch 32 teilen f�r CLK_ADC
	// Interne Spannungsreferenz w�hlen
	ADC0.CTRLC |= ADC_PRESC_DIV32_gc | ADC_REFSEL_INTREF_gc;

	// Temperatursensor f�r den AD Wandler konfigurieren
	ADC0.MUXPOS = ADC_MUXPOS_TEMPSENSE_gc;

	ADC0.CTRLD |= ADC_INITDLY_DLY64_gc;
	ADC0.SAMPCTRL = 64;
	ADC0.CTRLC |= ADC_SAMPCAP_bm;
	
	// ADC Peripherie aktivieren
	ADC0.CTRLA |= ADC_ENABLE_bm;

	v_UartInit(&SERIAL_TINY160X_PORT, SERIAL_TINY160X_TXD, SERIAL_TINY160X_RXD, USART_BAUD_115200);
}

/* Watchdog Timer starten*/
void v_WatchdogStart() {
	// Watchdog aktivieren mit 256ms Timeout
	ccp_write_io((void*)&WDT.CTRLA, WDT_PERIOD_256CLK_gc);
}

/* Watchdog Timer starten*/
void v_WatchdogStop() {
	// Watchdog deaktivieren
	ccp_write_io((void*)&WDT.CTRLA, 0);
}

/* Watchdog Timer zur�cksetzen */
void v_WatchdogReset() {
	__asm__ __volatile__ ("wdr");
}

/* Den Grund des Reset abfragen */
void v_GetResetReason() {
	uint8_t u8_ResetReason = RSTCTRL.RSTFR;

	v_UartSendString("Reset reason: ");

	if (u8_ResetReason & RESET_BROWN_OUT)
		v_UartSendString("Brown out");
	else if (u8_ResetReason & RESET_EXTERN)
		v_UartSendString("Extern");
	else if (u8_ResetReason & RESET_WATCHDOG)
		v_UartSendString("Watchdog");
	else if (u8_ResetReason & RESET_SOFTWARE)
		v_UartSendString("Software");
	else if (u8_ResetReason & RESET_UPDI)
		v_UartSendString("UPDI");
	else
		v_UartSendString("Power on");

	// Reset Flag l�schen
	RSTCTRL.RSTFR = u8_ResetReason;

	v_UartSendString("\r\n");
}

/* LED einschalten */
void v_setLedOn() {
	// LED einschalten
	LED_PORT.OUTCLR = LED_PIN;
}

/* LED ausschalten */
void v_setLedOff() {
	// LED ausschalten
	LED_PORT.OUTSET = LED_PIN;
}

/* Sega Gamepad Typ ermitteln */
void v_queryGamepadType(void) {
	uint8_t u8_temp, u8_temp2;
	uint8_t u8_counter = 50;

	v_UartSendString("Gamepad detection:");

	u8_GamepadType = SEGA_TYPE_3BUTTON;
	while (u8_counter > 0) {
		u8_counter--;

		// Warten bis sich die Signale stabilisiert haben, bevor die Pr�fung startet
		_delay_ms(100);

		// Watchdog Timer zur�cksetzen
		v_WatchdogReset();

		// Select Leitung auf 0 setzen
		SEGA_PORT_SEL.OUTCLR = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 1 setzen
		SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 0 setzen
		SEGA_PORT_SEL.OUTCLR = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 1 setzen
		SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 0 setzen
		SEGA_PORT_SEL.OUTCLR = SEGA_OUT_SEL;

		// Kurz warten bis sich die Signale stabilisiert haben
		_delay_us(4);

		u8_temp = SEGA_PORT1.IN;

		// Select Leitung auf 1 setzen
		SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 0 setzen
		SEGA_PORT_SEL.OUTCLR = SEGA_OUT_SEL;

		// Kurz warten bis sich die Signale stabilisiert haben
		_delay_us(4);
		u8_temp2 = SEGA_PORT1.IN;

		// Select Leitung auf 1 setzen
		SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

		u8_temp &= (SEGA_IN_D0 | SEGA_IN_D1 | SEGA_IN_D2 | SEGA_IN_D3);
		u8_temp2 &= (SEGA_IN_D0 | SEGA_IN_D1 | SEGA_IN_D2 | SEGA_IN_D3);

		if (u8_temp == 0 && u8_temp2 == (SEGA_IN_D0 | SEGA_IN_D1 | SEGA_IN_D2 | SEGA_IN_D3)) {
			// 6-Button Gamepad
			u8_GamepadType = SEGA_TYPE_6BUTTON;
			break;
		}
		else
			v_UartSendChar('.');
	}

	v_UartSendChar(' ');
	v_UartSendChar(u8_GamepadType + 0x30);
	v_UartSendString(" buttons\r\n");
}

/* Aufbereitung der Ausgabe der Kn�pfe */
void v_sendButtons(void) {
	char c_str[3];

	// Status der Kn�pfe in einem Nibble ausgeben
	v_Char2HexAscii(u8_SegaButtonUDLR, c_str);
	v_UartSendChar(c_str[1]);

	// Status der Kn�pfe in einem Byte ausgeben
	v_Char2HexAscii(u8_SegaButtonMZYXSCBA, c_str);
	v_UartSendString(c_str);
}

/* Sega Gamepad Kn�pfe abfragen */
void v_queryGamepadButtons(void) {
	uint8_t u8_temp1, u8_temp2;

	// Werte zur�cksetzen
	u8_SegaButtonUDLR = 0;
	u8_SegaButtonMZYXSCBA = 0;
	v_setLedOn();

	// Beide Gamepads mit 3+1 sowie 6+2 Kn�pfen werden zu Beginn identisch abgefragt
	u8_temp1 = SEGA_PORT1.IN;
	u8_temp2 = SEGA_PORT2.IN;
	if (u8_temp1 & SEGA_IN_D0)
		u8_SegaButtonUDLR |= SEGA_BT_UP;
	else
		v_setLedOff();

	if (u8_temp1 & SEGA_IN_D1)
		u8_SegaButtonUDLR |= SEGA_BT_DOWN;
	else
		v_setLedOff();

	if (u8_temp1 & SEGA_IN_D2)
		u8_SegaButtonUDLR |= SEGA_BT_LEFT;
	else
		v_setLedOff();

	if (u8_temp1 & SEGA_IN_D3)
		u8_SegaButtonUDLR |= SEGA_BT_RIGHT;
	else
		v_setLedOff();

	if (u8_temp2 & SEGA_IN_D4)
		u8_SegaButtonMZYXSCBA |= SEGA_BT_B;
	else
		v_setLedOff();

	if (u8_temp2 & SEGA_IN_D5)
		u8_SegaButtonMZYXSCBA |= SEGA_BT_C;
	else
		v_setLedOff();

	// Select Leitung auf 0 setzen
	SEGA_PORT_SEL.OUTCLR = SEGA_OUT_SEL;

	// Kurz warten bis sich die Signale stabilisiert haben
	_delay_us(2);

	u8_temp2 = SEGA_PORT2.IN;

	if (u8_temp2 & SEGA_IN_D4)
		u8_SegaButtonMZYXSCBA |= SEGA_BT_A;
	else
		v_setLedOff();

	if (u8_temp2 & SEGA_IN_D5)
		u8_SegaButtonMZYXSCBA |= SEGA_BT_START;
	else
		v_setLedOff();

	// Select Leitung auf 1 setzen
	SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

	if (u8_GamepadType == SEGA_TYPE_6BUTTON) {
		// bei dem Gamepad mit 6+2 Kn�pfen geht es jetzt hier weiter
		_delay_us(5);
		// Select Leitung auf 0 setzen
		SEGA_PORT_SEL.OUTCLR = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 1 setzen
		SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 0 setzen
		SEGA_PORT_SEL.OUTCLR = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 1 setzen
		SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

		// Kurz warten bis sich die Signale stabilisiert haben
		_delay_us(4);

		u8_temp1 = SEGA_PORT1.IN;

		// Select Leitung auf 0 setzen
		SEGA_PORT_SEL.OUTCLR = SEGA_OUT_SEL;

		_delay_us(5);
		// Select Leitung auf 1 setzen
		SEGA_PORT_SEL.OUTSET = SEGA_OUT_SEL;

		if (u8_temp1 & SEGA_IN_D0)
			u8_SegaButtonMZYXSCBA |= SEGA_BT_Z;
		else
			v_setLedOff();

		if (u8_temp1 & SEGA_IN_D1)
			u8_SegaButtonMZYXSCBA |= SEGA_BT_Y;
		else
			v_setLedOff();

		if (u8_temp1 & SEGA_IN_D2)
			u8_SegaButtonMZYXSCBA |= SEGA_BT_X;
		else
			v_setLedOff();

		// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//		if (u8_temp1 & SEGA_IN_D3)
//			u8_SegaButtonMZYXSCBA |= SEGA_BT_MODE;
//		else
//			v_setLedOff();
	}

//	sendButtons(gamepadType);
}

/* Kn�pfe Hoch/Runter/Links/Rechts ausgeben */
void v_directOutButtons(void) {
	if (b_showDebugOutput)
		v_UartSendChar('D');

	// Up/Down/Left/Right ausgeben
	if (u8_CD32ButtonUDLR & 0x01)
		CD32_PORT.OUTSET = CD32_OUT_UP;
	else
		CD32_PORT.OUTCLR = CD32_OUT_UP;

	if (u8_CD32ButtonUDLR & 0x02)
		CD32_PORT.OUTSET = CD32_OUT_DOWN;
	else
		CD32_PORT.OUTCLR = CD32_OUT_DOWN;

	if (u8_CD32ButtonUDLR & 0x04)
		CD32_PORT.OUTSET = CD32_OUT_LEFT;
	else
		CD32_PORT.OUTCLR = CD32_OUT_LEFT;

	if (u8_CD32ButtonUDLR & 0x08)
		CD32_PORT.OUTSET = CD32_OUT_RIGHT;
	else
		CD32_PORT.OUTCLR = CD32_OUT_RIGHT;

	if (u8_CD32ButtonSLRGYRB & 0x01)
		CD32_PORT.OUTSET = CD32_OUT_BLUE_DATA;
	else
		CD32_PORT.OUTCLR = CD32_OUT_BLUE_DATA;

	if (u8_CD32ButtonSLRGYRB & 0x02)
		CD32_PORT.OUTSET = CD32_INOUT_RED_CLOCK;
	else
		CD32_PORT.OUTCLR = CD32_INOUT_RED_CLOCK;
}

/* Kn�pfe Blau/Rot/Gelb/Gr�n/Vorne rechts/Vorne links/Pause ausgeben */
void v_shiftOutButtons(void) {
	uint8_t u8_temp, u8_counter;

	if (b_showDebugOutput)
		v_UartSendChar('S');

	// Die 7 Kn�pfe ausgeben
	u8_temp = u8_CD32ButtonSLRGYRB;
	for (u8_counter = 0; u8_counter < 7; u8_counter++) {
		if (u8_temp & 0x01)
			CD32_PORT.OUTSET = CD32_OUT_BLUE_DATA;
		else
			CD32_PORT.OUTCLR = CD32_OUT_BLUE_DATA;

		// N�chsten Knopf an die niedrigste Position schieben
		u8_temp >>= 1;

		// Auf die n�chste negative Flanke warten
		while((CD32_PORT.IN & CD32_IN_SEL) == 0 && (CD32_PORT.IN & CD32_INOUT_RED_CLOCK) != 0);
		// Auf die n�chste positive Flanke warten
		while((CD32_PORT.IN & CD32_IN_SEL) == 0 && (CD32_PORT.IN & CD32_INOUT_RED_CLOCK) == 0);
	}

	// Eine 1 ausgeben
	CD32_PORT.OUTSET = CD32_OUT_BLUE_DATA;
	// Auf die n�chste negative Flanke warten
	while((CD32_PORT.IN & CD32_IN_SEL) == 0 && (CD32_PORT.IN & CD32_INOUT_RED_CLOCK) != 0);
	// Auf die n�chste positive Flanke warten
	while((CD32_PORT.IN & CD32_IN_SEL) == 0 && (CD32_PORT.IN & CD32_INOUT_RED_CLOCK) == 0);

	// Eine 0 ausgeben
	CD32_PORT.OUTCLR = CD32_OUT_BLUE_DATA;
	// Auf die n�chste negative Flanke warten
	while((CD32_PORT.IN & CD32_IN_SEL) == 0 && (CD32_PORT.IN & CD32_INOUT_RED_CLOCK) != 0);
	// Auf die n�chste positive Flanke warten
	while((CD32_PORT.IN & CD32_IN_SEL) == 0 && (CD32_PORT.IN & CD32_INOUT_RED_CLOCK) == 0);

	// Eine weitere 0 ausgeben
	// Auf die n�chste negative Flanke warten
	while((CD32_PORT.IN & CD32_IN_SEL) == 0 && (CD32_PORT.IN & CD32_INOUT_RED_CLOCK) != 0);
	// Auf die n�chste positive Flanke warten
	while((CD32_PORT.IN & CD32_IN_SEL) == 0 && (CD32_PORT.IN & CD32_INOUT_RED_CLOCK) == 0);
}

/* Sega Kn�pfe den CD32 Kn�pfen zuordnen (gedr�ckte Kn�pfe live zuordnen) */
void v_mapSegaToCD32Buttons(void) {
	// Hoch/Runter/Links/Rechts Kn�pfe sind 1:1 gemappt
	u8_CD32ButtonUDLR = u8_SegaButtonUDLR;

	// Initialwert zur�cksetzen
	u8_CD32ButtonSLRGYRB = 0xFF;

	// Kn�pfe A,B,C,Start
	if (!(u8_SegaButtonMZYXSCBA & SEGA_BT_A))
		u8_CD32ButtonSLRGYRB &= ~u8_SegaMapBtA;
	if (!(u8_SegaButtonMZYXSCBA & SEGA_BT_B))
		u8_CD32ButtonSLRGYRB &= ~u8_SegaMapBtB;
	if (!(u8_SegaButtonMZYXSCBA & SEGA_BT_C))
		u8_CD32ButtonSLRGYRB &= ~u8_SegaMapBtC;
	if (!(u8_SegaButtonMZYXSCBA & SEGA_BT_START))
		u8_CD32ButtonSLRGYRB &= ~u8_SegaMapBtStart;

	if (u8_GamepadType == SEGA_TYPE_6BUTTON) {
		// Kn�pfe X,Y,Z,Mode
		if (!(u8_SegaButtonMZYXSCBA & SEGA_BT_X))
			u8_CD32ButtonSLRGYRB &= ~u8_SegaMapBtX;
		if (!(u8_SegaButtonMZYXSCBA & SEGA_BT_Y))
			u8_CD32ButtonSLRGYRB &= ~u8_SegaMapBtY;
		if (!(u8_SegaButtonMZYXSCBA & SEGA_BT_Z))
			u8_CD32ButtonSLRGYRB &= ~u8_SegaMapBtZ;
		// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//		if (!(u8_SegaButtonMZYXSCBA & SEGA_BT_MODE))
//			u8_CD32ButtonSLRGYRB &= ~u8_SegaMapBtMode;
	}
}

/* Internen Temperatursensor abfragen
 * return = Temperatur in Kelvin
 */
uint16_t v_getTemperature(void) {
	// AD-Wandlung starten
	ADC0.COMMAND = ADC_STCONV_bm;

	// Warten bis die Wandlung fertig ist
	while (!(ADC0.INTFLAGS & ADC_RESRDY_bm));

	// Flag zur�cksetzen
	ADC0.INTFLAGS = 0;

	// Read signed value from signature row
	int8_t sigrow_offset = SIGROW.TEMPSENSE1;
	// Read unsigned value from signature row
	uint8_t sigrow_gain = SIGROW.TEMPSENSE0;

	// ADC conversion result with 1.1 V internal reference
	uint16_t adc_reading = ADC0.RES;

	uint32_t temp = adc_reading - sigrow_offset;
	// Result might overflow 16 bit variable (10bit+8bit)
	temp *= sigrow_gain;
	 // Add 1/2 to get correct rounding on division below
	temp += 0x80;
	 // Divide result to get Kelvin
	temp >>= 8;

	return (uint16_t) temp;
}

/* Interne Seriennummer des Chips auslesen
 * pu8_Serial = Pointer auf ein Array, welches die Seriennummer enthalten soll
 */
void v_getDeviceSerial(uint8_t *pu8_Serial) {
	pu8_Serial[0] = SIGROW.SERNUM0;
	pu8_Serial[1] = SIGROW.SERNUM1;
	pu8_Serial[2] = SIGROW.SERNUM2;
	pu8_Serial[3] = SIGROW.SERNUM3;
	pu8_Serial[4] = SIGROW.SERNUM4;
	pu8_Serial[5] = SIGROW.SERNUM5;
	pu8_Serial[6] = SIGROW.SERNUM6;
	pu8_Serial[7] = SIGROW.SERNUM7;
	pu8_Serial[8] = SIGROW.SERNUM8;
	pu8_Serial[9] = SIGROW.SERNUM9;
}

/* Seriennummer ausgeben */
void v_ShowDeviceSerial() {
	uint8_t u8_Counter, u8_Serial[10];
	char c_Char[3];

	v_UartSendString("Device serial: ");
	v_getDeviceSerial(u8_Serial);

	for (u8_Counter = 0; u8_Counter < 10; u8_Counter++) {
		v_Char2HexAscii(u8_Serial[u8_Counter], c_Char);
		v_UartSendString(c_Char);
	}

	v_UartSendString("\r\n");
}

/* Daten ins EEProm �bernehmen */
void v_programEEprom(void) {
	_PROTECTED_WRITE_SPM(NVMCTRL.CTRLA, NVMCTRL_CMD_PAGEERASEWRITE_gc);
	while(NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm);
}

/* Byte in EEProm schreiben
 * u8_addr = Adresse innerhalb des EEProm
 * u8_data = Byte welches geschrieben werden soll
 * */
void v_storeEEprom(uint8_t u8_addr, uint8_t u8_data) {
	uint8_t *pu8_ptr = (uint8_t *)MAPPED_EEPROM_START;
	*(pu8_ptr + u8_addr) = u8_data;
	v_programEEprom();
}

/* Byte aus EEProm lesen
 * u8_addr = Adresse innerhalb des EEProm
 * return = Wert im EEProm
 *  */
uint8_t u8_readEEprom(uint8_t u8_addr) {
	while(NVMCTRL.STATUS & NVMCTRL_EEBUSY_bm);

	uint8_t *eep_ptr = (uint8_t *)MAPPED_EEPROM_START;
	return *(eep_ptr + u8_addr);
}

/* Reset durchf�hren */
static void v_reset(void) {
	// Warten bis keine Zeichen mehr gesendet werden
	while(!b_UartIsTxEmpty());
	_delay_ms(10);

	// Reset durchf�hren
	_PROTECTED_WRITE(RSTCTRL.SWRR, RSTCTRL_SWRE_bm);
}

/* Bootloader starten */
void v_enterBootloader(void) {
	// Watchdog Timer anhalten
	v_WatchdogStop();

	// Anforderung f�r den Bootloader speichern
	USERROW.USERROW31 = BOOTLOADER_REQUESTED;
	v_programEEprom();

	// Reset durchf�hren
	v_reset();
}

/* Knopf Wert zu Knopf Name aufl�sen
 * u8_button = Knopf Wert
 * return = Knopf Name
 */
void v_valueToButton(uint8_t u8_button) {
	if (u8_button == CD32_BT_BLUE)
		v_UartSendString(CD32_BT_NAME_BLUE);
	else if (u8_button == CD32_BT_RED)
		v_UartSendString(CD32_BT_NAME_RED);
	else if (u8_button == CD32_BT_YELLOW)
		v_UartSendString(CD32_BT_NAME_YELLOW);
	else if (u8_button == CD32_BT_GREEN)
		v_UartSendString(CD32_BT_NAME_GREEN);
	else if (u8_button == CD32_BT_FRONTR)
		v_UartSendString(CD32_BT_NAME_FRONTR);
	else if (u8_button == CD32_BT_FRONTL)
		v_UartSendString(CD32_BT_NAME_FRONTL);
	else if (u8_button == CD32_BT_START)
		v_UartSendString(CD32_BT_NAME_START);
}

/* Mapping aller Kn�pfe ausgeben */
void v_showButtonMapping(void) {
	v_UartSendString("Current button mapping (Sega=CD32):");

	v_UartSendString(" A=");
	v_valueToButton(u8_SegaMapBtA);
	v_UartSendString(", B=");
	v_valueToButton(u8_SegaMapBtB);
	v_UartSendString(", C=");
	v_valueToButton(u8_SegaMapBtC);
	if (u8_GamepadType == SEGA_TYPE_6BUTTON) {
		v_UartSendString(", X=");
		v_valueToButton(u8_SegaMapBtX);
		v_UartSendString(", Y=");
		v_valueToButton(u8_SegaMapBtY);
		v_UartSendString(", Z=");
		v_valueToButton(u8_SegaMapBtZ);
		// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//		v_UartSendString(", Mode=");
//		v_valueToButton(u8_SegaMapBtMode);
	}
	v_UartSendString(", Start=");
	v_valueToButton(u8_SegaMapBtStart);

	v_UartSendString("\r\n");
}

/* Sega Kn�pfe den CD32 Kn�pfen zuordnen (initiale Zuordnung) */
void v_setupSegaToCD32Buttons(void) {
	uint8_t u8_temp = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR);

	v_UartSendString("Loading button mapping: ");

	if (u8_temp == CUSTOM_BUTTON_MAPPING_FLAG) {
		// Benutzerdefinierte Belegung
		v_UartSendString("custom\r\n");
		u8_SegaMapBtA = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR + 1);
		u8_SegaMapBtB = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR + 2);
		u8_SegaMapBtC = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR + 3);
		u8_SegaMapBtX = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR + 4);
		u8_SegaMapBtY = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR + 5);
		u8_SegaMapBtZ = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR + 6);
		// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//		u8_SegaMapBtMode = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR + 7);
		u8_SegaMapBtStart = u8_readEEprom(CUSTOM_BUTTON_MAPPING_ADR + 8);
	}
	else {
		// Standardbelegung
		v_UartSendString("default\r\n");
		u8_SegaMapBtA = CD32_BT_RED;
		u8_SegaMapBtB = CD32_BT_BLUE;
		u8_SegaMapBtC = CD32_BT_GREEN;
		u8_SegaMapBtX = CD32_BT_YELLOW;
		u8_SegaMapBtY = CD32_BT_FRONTL;
		u8_SegaMapBtZ = CD32_BT_FRONTR;
		// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//		u8_SegaMapBtMode = CD32_BT_FRONTR;
		u8_SegaMapBtStart = CD32_BT_START;
	}

	v_showButtonMapping();
}

/* Knopf Name zu Knopf Wert aufl�sen
 * c_Button = Knopf name
 * return = Knopf Wert
 */
uint8_t u8_buttonToValue(char *c_Button) {
	// String zu Lowercase
	strlwr(c_Button);

	if (!strcmp(CD32_BT_NAME_BLUE, c_Button))
		return CD32_BT_BLUE;
	if (!strcmp(CD32_BT_NAME_RED, c_Button))
		return CD32_BT_RED;
	if (!strcmp(CD32_BT_NAME_YELLOW, c_Button))
		return CD32_BT_YELLOW;
	if (!strcmp(CD32_BT_NAME_GREEN, c_Button))
		return CD32_BT_GREEN;
	if (!strcmp(CD32_BT_NAME_FRONTR, c_Button))
		return CD32_BT_FRONTR;
	if (!strcmp(CD32_BT_NAME_FRONTL, c_Button))
		return CD32_BT_FRONTL;
	if (!strcmp(CD32_BT_NAME_START, c_Button))
		return CD32_BT_START;

	// Unbekannter Knopf
	return 0;
}

/* Zeige eine Fehlermeldung an, falls es beim Empfangen der Daten Fehler gab
 * u8_error = Fehlercode der seriellen Schnittstelle
 * return = true falls ein Fehler aufgetreten ist, ansonsten false
 */
bool b_showErrorCode(uint8_t u8_error) {
	if (u8_error != 0) {
		if (u8_error && SERIAL_ERROR_SW_BUFFER_OVERFLOW) {
			// Befehl ist zu lang
			v_UartClearRxBuffer();
			v_UartSendString("(ERR) Value too long\r\n");
		}
		else if (u8_error && SERIAL_ERROR_HW_BUFFER_OVERFLOW)
			// Hardware Puffer ist �bergelaufen
			v_UartSendString("(ERR) HW Buffer\r\n");
		else if (u8_error && SERIAL_ERROR_FRAME)
			// Frame wurde nicht erkannt
			v_UartSendString("(ERR) Frame\r\n");

		return true;
	}

	return false;
}

/* CD32 Knopf dem Sega Knopf zuordnen
 * c_ButtonName = Sega Knopf Bezeichnung
 * u8p_buttonMapping = Sega Knopf Variable
 * return = true falls alles geklappt hat, ansonsten false
 */
bool b_assignButton(char *c_ButtonName, uint8_t *u8p_buttonMapping) {
	uint8_t u8_counter;
	uint8_t u8_val = 0;
	uint8_t u8_error;
	char ac_command[MAX_LENGTH_RXBUF + 1];

	while (u8_val == 0) {
		v_UartSendString("\r\nCD32 button name to map on Sega button ");
		v_UartSendString(c_ButtonName);
		v_UartSendChar(':');

		// Alle Empfangenen Zeichen abfragen
		while (!b_UartIsNewlineDetected());

		// Pr�fen ob beim Empfang ein Fehler aufgetreten ist
		u8_error = u8_UartGetErrorCode();
		if (b_showErrorCode(u8_error))
			return false;

		u8_counter = 0;
		while (!b_UartIsRxEmpty())
			ac_command[u8_counter++] = u8_UartGetRxVal();
		// String Endezeichen erg�nzen
		ac_command[u8_counter] = 0;

		// Button ermitteln
		u8_val = u8_buttonToValue(ac_command);
		if (u8_val != 0) {
			v_UartSendString(ac_command);
			*u8p_buttonMapping = u8_val;
		}
		else {
			v_UartSendString("(ERR) Unknown button name, aborting button assignment\r\n");
			return false;
		}
	}

	return true;
}

/* Zuordnung der Kn�pfe im EEProm speichern */
void v_storeNewButtonAssignment(void) {
	// Festhalten, dass es eine g�ltige neue Zuordnung der Kn�pfe gibt
	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR, CUSTOM_BUTTON_MAPPING_FLAG);

	// Alle Zuordnungen im EEProm speichern
	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR + 1, u8_SegaMapBtA);
	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR + 2, u8_SegaMapBtB);
	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR + 3, u8_SegaMapBtC);
	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR + 4, u8_SegaMapBtX);
	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR + 5, u8_SegaMapBtY);
	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR + 6, u8_SegaMapBtZ);
	// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR + 7, u8_SegaMapBtMode);
	v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR + 8, u8_SegaMapBtStart);
}

/* Serielle Befehle verarbeiten */
void v_commandParser(void) {
	uint8_t u8_counter = 0;
	uint8_t u8_error;
	uint8_t u8_buffer[8];
	char ac_command[MAX_LENGTH_RXBUF + 1];

	u8_error = u8_UartGetErrorCode();
	if (!b_showErrorCode(u8_error)) {
		// Alle Empfangenen Zeichen abfragen
		u8_counter = 0;
		while (!b_UartIsRxEmpty())
			ac_command[u8_counter++] = u8_UartGetRxVal();
		// String Endezeichen erg�nzen
		ac_command[u8_counter] = 0;

		// String zu Lowercase
		strlwr(ac_command);

		if (!strcmp(CMD_HELP, ac_command)) {
			v_UartSendString(CMD_HELP);
			v_UartSendString("=Help\r\n");
			v_UartSendString(CMD_DEBUG);
			v_UartSendString("=De-/activate debug output\r\n");
			v_UartSendString(CMD_BOOTLOADER);
			v_UartSendString("=Start bootloader\r\n");
			v_UartSendString(CMD_RESET);
			v_UartSendString("=Reset\r\n");
			v_UartSendString(CMD_VERSION);
			v_UartSendString("=Show version\r\n");
			v_UartSendString(CMD_SERIAL);
			v_UartSendString("=Show chip serial\r\n");
//			v_UartSendString(CMD_TEMPERATURE);
//			v_UartSendString("=Show temperature in Kelvin\r\n");
			v_UartSendString(CMD_BUTTON_SHOW_MAPPING);
			v_UartSendString("=Show current button mapping\r\n");
			v_UartSendString(CMD_BUTTON_SET_MAPPING);
			v_UartSendString("=Set custom button mapping\r\n");
			v_UartSendString(CMD_BUTTON_CLR_MAPPING);
			v_UartSendString("=Set default button mapping\r\n");
			v_UartSendString(CMD_DETECT_GAMEPAD);
			v_UartSendString("=Restart gamepad detection\r\n");
		}
		else if (!strcmp(CMD_DEBUG, ac_command)) {
			// Debug Ausgabe ein-/ausschalten
			if (b_showDebugOutput) {
				v_UartSendString("Turn debug off\r\n");
				b_showDebugOutput = false;
			}
			else {
				v_UartSendString("Turn debug on\r\n");
				b_showDebugOutput = true;
			}
		}
		else if (!strcmp(CMD_BOOTLOADER, ac_command)) {
			// Bootloader starten
			v_UartSendString("Start Bootloader\r\n");
			v_enterBootloader();
		}
		else if (!strcmp(CMD_RESET, ac_command)) {
			// Bootloader starten
			v_UartSendString("Reset\r\n");
			v_reset();
		}
		else if (!strcmp(CMD_VERSION, ac_command)) {
			// Version anzeigen
			v_UartSendString("Version: ");
			v_UartSendString(VERSION);
			v_UartSendString("\r\n");
		}
		else if (!strcmp(CMD_SERIAL, ac_command))
			// Chip Seriennummer anzeigen
			v_ShowDeviceSerial();
		else if (!strcmp(CMD_TEMPERATURE, ac_command)) {
			// Temperatur abfragen
			char ac_temp[6];
			uint16_t u16_temp = v_getTemperature();

			itoa(u16_temp, ac_temp, 10);

			v_UartSendString("Temperature: ");
			v_UartSendString(ac_temp);
			v_UartSendString(" K\r\n");
		}
		else if (!strcmp(CMD_BUTTON_SHOW_MAPPING, ac_command))
			v_showButtonMapping();
		else if (!strcmp(CMD_BUTTON_SET_MAPPING, ac_command)) {
			// Benutzerdefinierte Zuordnung der Kn�pfe
			v_UartSendString("Enter CD32 button name for each Sega button\r\n");
			v_UartSendString("Possible values: blue,red,yellow,green,front_l,front_r,start\r\n");

			// Watchdog Timer vor�bergehend pausieren
			v_WatchdogStop();

			// Mapping abfragen
			if (!b_assignButton("A", &u8_buffer[0])) {
				// Watchdog Timer wieder starten
				v_WatchdogStart();
				return;
			}
			if (!b_assignButton("B", &u8_buffer[1])) {
				// Watchdog Timer wieder starten
				v_WatchdogStart();
				return;
			}
			if (!b_assignButton("C", &u8_buffer[2])) {
				// Watchdog Timer wieder starten
				v_WatchdogStart();
				return;
			}
			if (u8_GamepadType == SEGA_TYPE_6BUTTON) {
				if (!b_assignButton("X", &u8_buffer[3])) {
					// Watchdog Timer wieder starten
					v_WatchdogStart();
					return;
				}
				if (!b_assignButton("Y", &u8_buffer[4])) {
					// Watchdog Timer wieder starten
					v_WatchdogStart();
					return;
				}
				if (!b_assignButton("Z", &u8_buffer[5])) {
					// Watchdog Timer wieder starten
					v_WatchdogStart();
					return;
				}
				// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//				if (!b_assignButton("Mode", &u8_buffer[6]))
//					return;
			}
			else {
				// Kn�pfe werden nicht verwendet und deshalb mit default Werten belegt
				u8_SegaMapBtX = CD32_BT_YELLOW;
				u8_SegaMapBtY = CD32_BT_FRONTL;
				u8_SegaMapBtZ = CD32_BT_FRONTR;
				// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//				u8_SegaMapBtMode = CD32_BT_FRONTR;
			}
			if (!b_assignButton("Start", &u8_buffer[7])) {
				// Watchdog Timer wieder starten
				v_WatchdogStart();
				return;
			}

			// Mapping zuordnen, falls kein Fehler aufgetreten ist
			u8_SegaMapBtA = u8_buffer[0];
			u8_SegaMapBtB = u8_buffer[1];
			u8_SegaMapBtC = u8_buffer[2];
			if (u8_GamepadType == SEGA_TYPE_6BUTTON) {
				u8_SegaMapBtX = u8_buffer[3];
				u8_SegaMapBtY = u8_buffer[4];
				u8_SegaMapBtZ = u8_buffer[5];
				// Problem: Knopf schaltet in Verbindung mit anderen Kn�pfen den Modus auf 3-Button um
//				u8_SegaMapBtMode = u8_buffer[6];
			}			
			u8_SegaMapBtStart = u8_buffer[7];

			v_UartSendString("\r\nSaving new assignment:");
			v_storeNewButtonAssignment();
			v_UartSendString("OK\r\n");

			// Watchdog Timer wieder starten
			v_WatchdogStart();
		}
		else if (!strcmp(CMD_BUTTON_CLR_MAPPING, ac_command)) {
			// Standard Zuordnung der Kn�pfe wiederherstellen
			v_storeEEprom(CUSTOM_BUTTON_MAPPING_ADR, 0xFF);
			v_setupSegaToCD32Buttons();
		}
		else if (!strcmp(CMD_DETECT_GAMEPAD, ac_command)) {
			// Gamepad Typ erneut abfragen
			v_queryGamepadType();
			v_setupSegaToCD32Buttons();
		}
		else {
			// Unbekannter Befehl
			v_UartSendString("(ERR) Unknown CMD: ");
			v_UartSendString(ac_command);
			v_UartSendString("\r\n");
		}
	}
}

int main(void) {
	init();

	// Watchdog Timer starten
	v_WatchdogStart();

	v_GetResetReason();

	v_UartSendString("Starting CD32 Gamepad Emulator ");
	v_UartSendString(VERSION);
	v_UartSendString("\r\n");

	v_UartSendString(COPYRIGHT);

	v_UartSendString("Show help with '");
	v_UartSendString(CMD_HELP);
	v_UartSendString("'\r\n");

	// Gamepad Typ abfragen
	v_queryGamepadType();

	v_setupSegaToCD32Buttons();

	// LED einschalten
	v_setLedOn();

	sei();

	while(1) {
		// Sega Gamepad Kn�pfe abfragen
		v_queryGamepadButtons();

		// Sega Kn�pfe den CD32 Kn�pfen zuordnen
		v_mapSegaToCD32Buttons();

		// Kn�pfe Hoch/Runter/Links/Rechts/Blau/Rot ausgeben
		v_directOutButtons();

		if (b_UartIsNewlineDetected())
			v_commandParser();

		// Watchdog Timer zur�cksetzen
		v_WatchdogReset();
		_delay_ms(4);
	}
}

/******************************************************** Interruptfunktionen ********************************************************/

ISR(CD32_PORT_ISR) {
	// Interrupt deaktivieren
	cli();

	// P6 als Eingang konfigurieren
	CD32_PORT.DIRCLR = CD32_INOUT_RED_CLOCK;

	// Kn�pfe ausgeben
	v_shiftOutButtons();

	// Interrupt Flag l�schen
	CD32_PORT.INTFLAGS &= CD32_IN_SEL;

	// Warten bis P5 = 1 gesetzt wird
	while((CD32_PORT.IN & CD32_IN_SEL) == 0);
	// P6 als Ausgang konfigurieren
	CD32_PORT.DIRSET = CD32_INOUT_RED_CLOCK;

	// Interrupt aktivieren
	sei();
}

// Serieller RX Interrupt
ISR(SERIAL_RX_ISR) {
	while (SERIAL_STATUS & USART_RXCIF_bm)
		v_UartSetRxVal(SERIAL_RX_DATAH, SERIAL_RX_DATA);
}

// Serieller TX Interrupt
ISR(SERIAL_TX_ISR) {
	// Interrupt Flag zur�cksetzen
	SERIAL_STATUS |= USART_TXCIF_bm;

	if (!b_UartIsTxEmpty())
		// Sendeprozess starten
		SERIAL_TX_DATA = u8_UartGetTxVal();
	else
		b_UartTxInProgress = false;
}
