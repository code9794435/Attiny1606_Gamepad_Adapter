Changelog

v1.4.5
Info: Uart Lib umgebaut auf Port�bergabe
Info: Copyright und Lizent erg�nzt
Info: Textausgabe von LF auf CRLF ge�ndert
Info: Kurze Wartezeit erg�nzt um UART Ausgabe sicher zu stellen

v1.4.4
Bugfix: Die nachtr�gliche Detektion des Gamepads �ber den Befehl "detect" korrigiert
Feature: Anzeige des Grunds weshalb der Controller gestartet wurde (Power-On, Brown-Out, Watchdog, Extern, Software, UPDI)
Feature: Watchdog erg�nzt um den Controller automatisch neu zu starten, falls dieser h�ngt (kann durch Spannungsschwankungen auftreten)
Info: Code Optimierungen

v1.4.3
Bugfix: Die neue Erkennung des Gamepads in v1.4.2 l�uft bei Gamepads mit 3 Kn�pfen in eine Endlosschleife
Feature: Neue Funktion "serial" um die Chip Seriennummer abzufragen

v1.4.2
Bugfix: Erkennungsroutine des Gamepads wiederholt ausf�hren um auch sp�t reagierende Gamepads korrekt zu erkennen (z.B. Funk-Gamepads)

v1.4.1
Bugfix: Zuordnung der Kn�fe und dessen Handling optimiert
Bugfix: Falsche Pin-Initialisierung korrigiert
Bugfix: Sega Mode Knopf aus Zuordnung entfernt wegen Umschaltung in 3-Button Mode
Info: Befehle und Methoden umbenannt
Info: Firmware Doku angepasst
