// Copyright (c) 2022 - 2023 by Rainer Wahler, RWahler@gmx.net

#ifndef MAIN_H
	#define	MAIN_H

	#define VERSION "V1.4.5"
	#define COPYRIGHT "Programmed in Licence by Alinea Computer www.amiga-shop.net\n(c) by Rainer Wahler (RWahler@gmx.net)\r\n"

	#define CMD_HELP "help"
	#define CMD_RESET "reset"
	#define CMD_DEBUG "debug"
	#define CMD_BOOTLOADER "boot"
	#define CMD_VERSION "version"
	#define CMD_SERIAL "serial"
	#define CMD_TEMPERATURE "temp"
	#define CMD_BUTTON_SHOW_MAPPING "show_btn"
	#define CMD_BUTTON_SET_MAPPING "set_btn"
	#define CMD_BUTTON_CLR_MAPPING "clear_btn"
	#define CMD_DETECT_GAMEPAD "detect"

	#define BOOTLOADER_REQUESTED 0xEB
	#define CUSTOM_BUTTON_MAPPING_ADR 0x00
	#define CUSTOM_BUTTON_MAPPING_FLAG 0x1C

	#define LED_PORT PORTC
	#define LED_PIN PIN3_bm

	#define SEGA_PORT_SEL PORTC
	#define SEGA_OUT_SEL PIN0_bm

	#define SEGA_PORT1 PORTB
	#define SEGA_IN_D0 PIN0_bm
	#define SEGA_IN_D1 PIN1_bm
	#define SEGA_IN_D2 PIN4_bm
	#define SEGA_IN_D3 PIN5_bm

	#define SEGA_PORT2 PORTC
	#define SEGA_IN_D4 PIN1_bm
	#define SEGA_IN_D5 PIN2_bm

	#define SEGA_BT_UP 0x01
	#define SEGA_BT_DOWN 0x02
	#define SEGA_BT_LEFT 0x04
	#define SEGA_BT_RIGHT 0x08
	#define SEGA_BT_A 0x01
	#define SEGA_BT_B 0x02
	#define SEGA_BT_C 0x04
	#define SEGA_BT_START 0x08
	#define SEGA_BT_X 0x10
	#define SEGA_BT_Y 0x20
	#define SEGA_BT_Z 0x40
	#define SEGA_BT_MODE 0x80

	#define CD32_PORT_ISR PORTA_PORT_vect
	#define CD32_IN_SEL_ISR PIN5CTRL
	#define CD32_PORT PORTA
	#define CD32_OUT_UP PIN1_bm
	#define CD32_OUT_DOWN PIN2_bm
	#define CD32_OUT_LEFT PIN3_bm
	#define CD32_OUT_RIGHT PIN4_bm
	#define CD32_IN_SEL PIN5_bm
	#define CD32_INOUT_RED_CLOCK PIN6_bm
	#define CD32_OUT_BLUE_DATA PIN7_bm

	#define CD32_BT_BLUE 0x01
	#define CD32_BT_RED 0x02
	#define CD32_BT_YELLOW 0x04
	#define CD32_BT_GREEN 0x08
	#define CD32_BT_FRONTR 0x10
	#define CD32_BT_FRONTL 0x20
	#define CD32_BT_START 0x40

	#define CD32_BT_NAME_BLUE "blue"
	#define CD32_BT_NAME_RED "red"
	#define CD32_BT_NAME_YELLOW "yellow"
	#define CD32_BT_NAME_GREEN "green"
	#define CD32_BT_NAME_FRONTR "front_r"
	#define CD32_BT_NAME_FRONTL "front_l"
	#define CD32_BT_NAME_START "start"

	#define SEGA_TYPE_3BUTTON 3
	#define SEGA_TYPE_6BUTTON 6

	#define RESET_POWER_ON 1
	#define RESET_BROWN_OUT 2
	#define RESET_EXTERN 4
	#define RESET_WATCHDOG 8
	#define RESET_SOFTWARE 16
	#define RESET_UPDI 32

#endif	/* MAIN_H */
